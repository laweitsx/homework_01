#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

    
		
    int w;
    int h;
    float intervalX;
    float intervalY;
    float recY;
    float recX;
    float tilt;
    float rhytm;
    float frameCount;
    
    ofxPanel gui;
    ofxIntSlider num;
    ofxIntSlider background;
    ofxToggle filled;
};
