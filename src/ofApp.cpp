#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofEnableAntiAliasing();
    ofEnableSmoothing();
    ofSetCircleResolution(100);
    recX = 100;
    recY = 200;
    intervalX = 0;
    intervalY = 0;
    tilt = 0;
    rhytm = 0;
    frameCount = 0;
    w = 640;
    h = 480;
    
    gui.setup();
    gui.add(num.setup("Rec numbers", 10, 5, 15));
    gui.add(background.setup("Background color", 255, 0, 255));
    gui.add(filled.setup("fill",true));
    
    
    
    
    
    
    
}

//--------------------------------------------------------------
void ofApp::update(){
   
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofClear(0);
    frameCount++;
    ofBackground(background, 251, 249);
    gui.draw();
    ofTranslate(w/2, h/2);
    intervalX = ofMap(ofGetMouseX(), 0, w, 40, -40);
    intervalY = ofMap(abs(ofGetMouseX()-w/2), 0, w/2, 0, -20);
    tilt = ofMap(ofGetMouseX(), 0, w, -20, 20);
    ofSetRectMode(OF_RECTMODE_CENTER);
    ofFill();
    ofSetColor(20);
    if(filled){
        
        for(int i = num-1; i > 0; i--){
            ofPushMatrix();
            rhytm = ofMap(pow(abs(sin(frameCount*0.03 - i*0.3)), 50), 0, 1, 0, -50) * ofMap(abs(ofGetMouseX() - w / 2), 0, w/2, 0, 1);
            ofTranslate(intervalX * (i - num / 2.0), intervalY * (i - num / 2.0)+rhytm);
            ofBeginShape();
            ofVertex(-recX / 2.0, -recY / 2.0 + tilt);
            ofVertex(recX / 2.0, -recY / 2.0 - tilt);
            ofVertex(recX / 2.0, recY / 2.0 - tilt);
            ofVertex(-recX / 2.0, recY / 2.0 + tilt);
            ofEndShape();
            ofPopMatrix();
            
        }
        
    }else{
        for(int i = num-1; i > 0; i--){
            ofPushMatrix();
            rhytm = ofMap(pow(abs(sin(frameCount*0.03 - i*0.3)), 50), 0, 1, 0, -50) * ofMap(abs(ofGetMouseX() - w / 2), 0, w/2, 0, 1);
            ofTranslate(intervalX * (i - num / 2.0), intervalY * (i - num / 2.0)+rhytm);

            ofNoFill();
            ofSetColor(255, 251, 249);
            ofSetLineWidth(4);
            ofBeginShape();
            ofVertex(-recX / 2.0, -recY / 2.0 + tilt);
            ofVertex(recX / 2.0, -recY / 2.0 - tilt);
            ofVertex(recX / 2.0, recY / 2.0 - tilt);
            ofVertex(-recX / 2.0, recY / 2.0 + tilt);
            ofVertex(-recX / 2.0, -recY / 2.0 + tilt);
            ofEndShape();
            ofPopMatrix();
            
        }
    }
    
    
    
    

    
    
}

